# What is GraphIP UI?

GraphIP UI is a set of essential modules for your next React application.

The purpose of the modules is to solve generic tasks faster and more efficient so that you can focus on business logic and leave routine behind.

## What's included

GraphIP UI `npm` package contain three things:

- GraphIP UI Theme `graphip-ui/theme`
  - Theme System - set of `javaScript` object, which allows you to modify application look & feel by changing variables, with fewer custom styles.
  - Responsive breakpoints - have function to use in your `Styled Components` breakpointUp breakpointDown breakpointBetween breakpointOnly.
  - Server-side rendering compatibility!
  - Right-to-left writing system support for all components.
- GraphIP UI Components `graphip-ui`
  - Global components (Layout-Card-FlipCard-RevealCard-Accordion-List).
  - Navigation components (Sidebar-Menu-Tabs-Actions).
  - Forms components (InputGroup-Radio-Select-Checkbox-Button).
  - Grid components (Container-Row-Col).
  - Modals & Overlays components (Popover-Toastr-Tooltip-ContextMenu).
  - Extra components (Search-User-Badge-Alert-ProgressBar-ChatUI-Spinner).
- GraphIP UI Svg `graphip-ui/svg`
  - This svg components ready to use.
