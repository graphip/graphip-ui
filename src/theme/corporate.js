/*
 * @license
 * Copyright Aigis Services Ltd. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

export default {
  fontSize: '14px',
	
  headerFg: '#f7fafb',
  headerBg: '#292f38',

  layoutBg: '#ffffff',
  layoutFg: 'none',

  colorFgHeading: '#181818',
  colorFgText: '#c7d1db',
  colorFgHighlight: '#ff0040',

  separator: '#3c424c',

  radius: '0.17rem',  

  scrollbarBg: '#e3e9ee',

  colorPrimary: '#73a1ff',
  colorDark: '#2b323b',
  colorSuccess: '#5dcfe3',
  colorInfo: '#ba7fec',
  colorWarning: '#ffa36b',
  colorDanger: '#ff6b83',

  btnSecondaryBg: '#edf2f5',
  btnSecondaryBorder: '#edf2f5',

  actionsFg: '#d3dbe5',
  actionsBg: 'colorBg',

  sidebarBg: '#292f38',
  sidebarShadow: 'none',

  borderColor: '#d5dbe0',
  borderColorDark: '#252c36',

  menuFontWeight: 'fontWeightBolder',
  menuFontSize: 'fontSize',
  menuFg: 'colorFgText',
  menuBg: '#292f38',
  menuActiveFg: '#ff0040',  
  menuActiveBg: 'menuBg',
  menuSubmenuBg: 'menuBg',
  menuSubmenuFg: 'colorFgText',
  menuSubmenuActiveFg: 'colorFgHeading',
  menuSubmenuActiveBg: '#cdd5dc',
  menuSubmenuActiveBorderColor: 'menuSubmenuActiveBg',
  menuSubmenuActiveShadow: 'none',
  menuSubmenuHoverFg: 'menuSubmenuActiveFg',
  menuSubmenuHoverBg: 'menuBg',
  menuSubmenuItemBorderWidth: '0.125rem',
  menuSubmenuItemBorderRadius: 'radius',
  menuSubmenuItemPadding: '0.5rem 1rem',
  menuSubmenuItemContainerPadding: '0 1.25rem',  
  menuSubmenuPadding: '0.5rem',
  menuItemSeparator: 'transparent',
  menuIconColor: '#626973',
  menuIconActiveColor: '#ff0040',
  
  contextMenuFg: 'colorFgText',
  contextMenuBorder: 'borderColorDark',
  contextMenuBorderRadius: '0.25rem',
  contextMenuBg: 'colorDark',
  contextMenuActiveBg: 'transparent',
  contextMenuIconColor: 'menuIconColor',
  contextMenuShadow: '0 3px 10px rgba(36,43,53,.9)',
  

  btnBorderRadius: 'btnSemiRoundBorderRadius',
  btnHeroDegree: '0deg',
  btnHeroGlowSize: '0 0 20px 0',
  btnHeroSecondaryGlowSize: '0 0 0 0',
  btnHeroBorderRadius: 'btnBorderRadius',

  cardShadow: 'none',
  cardBorderWidth: '1px',
  cardBorderColor: 'borderColor',
  cardHeaderBorderWidth: 0,

  linkColor: '#5dcfe3',
  linkColorHover: '#7dcfe3',
  linkColorVisited: 'linkColor',

  actionsSeparator: '#f1f4f5',

  modalSeparator: 'borderColor',

  tabsSelected: 'colorPrimary',
  tabsSelectedSecondColor: 'colorSuccess',

  smartTablePagingBgActive: 'colorPrimary',

  routeTabsSelected: 'colorPrimary',

  popoverBorder: 'colorPrimary',

  footerShadow: 'none',
  footerPadding: 0,
  footerMargin: '0.5rem 0 0 0',

  footerSeparator: 'transparent',
  footerFgHighlight: '#2a2a2a',

  calendarTodayItemBg: '#a2b2c7',
  calendarActiveItemBg: 'colorPrimary',
  calendarRangeBgInRange: '#e3ecfe',
  calendarTodayFg: 'colorWhite',

  toastrIconRadius: 'radius',

  datepickerBorder: 'colorPrimary'
};
